/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/* ==================================================================== */
#ifndef __DDE_H__
#define __DDE_H__
/* ==================================================================== */
#include <windows.h>
/* ==================================================================== */
#define DEFAULT_DDE_TIMEOUT 3000
/* ==================================================================== */
HDDEDATA CALLBACK DdeCallback(UINT uType,
    UINT uFmt,
    HCONV hconv,
    HSZ hsz1,
    HSZ hsz2,
    HDDEDATA hdata,
    DWORD dwData1,
    DWORD dwData2);

BOOL DDEExecute(const wchar_t* szCommand, int TimeOut);

wchar_t *DDERequest(const wchar_t* szItem, int TimeOut, BOOL bCleanResult, int *lenResult, BOOL *iErr);

BOOL DDEPoke(const wchar_t* szItem, const wchar_t* szData, int TimeOut);

BOOL DDEOpenConnection(const wchar_t *strServeur, const wchar_t *strTopic);

BOOL DDECloseConnection(void);

BOOL DDEIsOpen(void);

/* ==================================================================== */
#endif /* __DDE__ */

