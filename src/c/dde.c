/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/* ==================================================================== */
#include <windows.h>
#include <ddeml.h>
#include "dde.h"
#include "sci_malloc.h"
#include "BOOL.h"
/* ==================================================================== */
#pragma comment(lib,"User32.lib")
/* ==================================================================== */
static DWORD idInst = 0;
static HCONV hConv = NULL;
static BOOL DDEInUse = FALSE;
/* ==================================================================== */
HDDEDATA CALLBACK DdeCallback(
    UINT uType,     // Transaction type.
    UINT uFmt,      // Clipboard data format.
    HCONV hconv,    // Handle to the conversation.
    HSZ hsz1,       // Handle to a string.
    HSZ hsz2,       // Handle to a string.
    HDDEDATA hdata, // Handle to a global memory object.
    DWORD dwData1,  // Transaction-specific data.
    DWORD dwData2)  // Transaction-specific data.
{
    return 0;
}
/* ==================================================================== */
BOOL DDEIsOpen(void)
{
    return DDEInUse;
}
/* ==================================================================== */
BOOL DDEOpenConnection(const wchar_t *strServeur, const wchar_t *strTopic)
{
    UINT iReturn = DMLERR_NO_ERROR;
    BOOL bOK = TRUE;

    if (DDEInUse)
    {
        DDECloseConnection();
    }

    iReturn = DdeInitialize(&idInst,
        (PFNCALLBACK)DdeCallback,
        APPCLASS_STANDARD | APPCMD_CLIENTONLY,
        0 );

    if (iReturn!=DMLERR_NO_ERROR)
    {
        bOK = FALSE;
    }
    else
    {
        HSZ hszApp, hszTopic;

        hszApp = DdeCreateStringHandleW(idInst, strServeur, 0);
        hszTopic = DdeCreateStringHandleW(idInst, strTopic, 0);
        hConv = DdeConnect(idInst, hszApp, hszTopic, NULL);

        DdeFreeStringHandle(idInst, hszApp);
        DdeFreeStringHandle(idInst, hszTopic);

        if (hConv == NULL)
        {
            DdeUninitialize(idInst);
            bOK = FALSE;
        }

        DDEInUse = TRUE;
    }

    return bOK;
}
/* ==================================================================== */
BOOL DDECloseConnection(void)
{
    BOOL bOK = FALSE;

    if (DDEInUse)
    {
        DDEInUse = FALSE;
        DdeDisconnect(hConv);
        DdeUninitialize(idInst);
        idInst = 0;
        hConv = NULL;
        bOK = TRUE;
    }
    else
    {
        bOK = FALSE;
    }
    return bOK;
}
/* ==================================================================== */
BOOL DDEExecute(const wchar_t* szCommand, int TimeOut)
{
    BOOL bOK = TRUE;
    DWORD len = (wcslen(szCommand) + 1) * sizeof(wchar_t);

    HDDEDATA hData = DdeCreateDataHandle(idInst,
        (LPBYTE)szCommand, 
        len,
        0,
        NULL,
        CF_UNICODETEXT,
        0);
    if (hData == NULL)
    {
        bOK = FALSE;
    }
    else
    {
        HDDEDATA ReturnValue = NULL;
        bOK = TRUE;
        if (TimeOut > 0)
        {
            TimeOut = TIMEOUT_ASYNC;
        }

        ReturnValue = DdeClientTransaction((LPBYTE)hData,
            0xFFFFFFFF,
            hConv,
            0L,
            0,
            XTYP_EXECUTE,
            TimeOut, 
            NULL);

        if (!ReturnValue) bOK = FALSE;
    }
    return bOK;
}
/* ==================================================================== */
wchar_t *DDERequest(const wchar_t* szItem, int TimeOut, BOOL bCleanResult, int *lenResult, BOOL *iErr)
{
    wchar_t *Result = NULL;
    HDDEDATA hData = NULL;
    DWORD len = 0;

    HSZ hszItem = DdeCreateStringHandleW(idInst, szItem, 0);

    *lenResult = 0;

    if ( TimeOut < 0 )
    {
        TimeOut = DEFAULT_DDE_TIMEOUT;
    }

    hData = DdeClientTransaction(NULL,
        0,
        hConv,
        hszItem,
        CF_UNICODETEXT,
        XTYP_REQUEST,
        TimeOut,
        NULL);

    if (hData != NULL)
    {
        DWORD length = DdeGetData(hData, NULL, 0, 0);
        if (length > 0)
        {
            Result = (wchar_t*)MALLOC(sizeof(wchar_t) * (length + 1));
            *lenResult = length;
        }

        if (Result)
        {
            DdeGetData(hData, (LPBYTE)Result, length, 0);
            if ( DdeGetLastError(idInst) == DMLERR_NO_ERROR )
            {
                if (Result && bCleanResult)
                {
                    if (*lenResult > 2)
                    {
                        // remove '\r\n'
                        int pos = *lenResult - 1;
                        if (pos > 0)
                        {
                            if (Result[*lenResult - 1] == L'\n')
                            {
                                Result[*lenResult - 1] = L'\0';
                            }
                        }
                        *lenResult = (int) wcslen(Result);
                        pos = *lenResult - 2;
                        if (pos > 0)
                        {
                            if (Result[*lenResult - 2] == L'\r')
                            {
                                Result[*lenResult - 2] = L'\0';
                            }
                        }

                        *lenResult = (int) wcslen(Result);
                    }
                }
                *iErr = TRUE;
                return Result;
            }
            FREE(Result);
            Result = NULL;
        }
    }

    *iErr = FALSE;
    return Result;
}
/* ==================================================================== */
BOOL DDEPoke(const wchar_t* szItem, const wchar_t* szData, int TimeOut)
{
    BOOL bOK = TRUE;
    HDDEDATA ReturnValue = NULL;
    DWORD lenSzData = (wcslen(szData) + 1) * sizeof(wchar_t);

    HSZ hszItem = DdeCreateStringHandleW(idInst, szItem, 0);
    if ( TimeOut < 0 )
    {
        TimeOut = DEFAULT_DDE_TIMEOUT;
    }

    ReturnValue = DdeClientTransaction((LPBYTE)szData,
        lenSzData,
        hConv,
        hszItem,
        CF_UNICODETEXT,
        XTYP_POKE,
        TimeOut,
        NULL);

    if (!ReturnValue) bOK = FALSE;
    if (!DdeFreeStringHandle(idInst, hszItem)) bOK = FALSE;

    return bOK;
}
/* ==================================================================== */
