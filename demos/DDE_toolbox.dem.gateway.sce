// ====================================================================
// Copyright DIGITEO 2008 - 2011
// Allan CORNET
// ====================================================================

function subdemolist = demos_gw(demopath)

demopath = get_absolute_file_path("DDE_toolbox.dem.gateway.sce");

subdemolist = ["demo DDE - read excel file"       , "DDE_read_excel.dem.sce";
               "demo DDE - write excel file"      , "DDE_write_excel.dem.sce";
               "demo DDE - message box from excel", "DDE_msgbox_excel.dem.sce"];

subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction
// ====================================================================
subdemolist = demos_gw();
clear demos_gw;
// ====================================================================
