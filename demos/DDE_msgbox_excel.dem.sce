// ====================================================================
// Copyright DIGITEO 2008
// Allan CORNET
// ====================================================================

lines(0);
clc();

try
  winopen('Excel');
catch
  error(999,'Excel not found');
  return;
end

// Wait Excel
sleep(2000);

disp('Initialize Connection with Excel');
res = ddeopen('Excel','');

if res then 

  disp('Create a message box from Excel');
  ddeexec('[APP.ACTIVATE][ALERT(""Hello from the sample Excel DDE driver"")]')
	
	disp('Quit Excel');
	ddeexec('[Quit()]');
	
	disp('Close Connection with Excel');
	ddeclose();
end
// ====================================================================
