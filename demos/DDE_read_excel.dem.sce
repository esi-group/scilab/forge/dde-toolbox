// ====================================================================
// Copyright DIGITEO 2008 - 2011
// Allan CORNET
// ====================================================================
function demo_read_excel()

  clc();
  try
    demo_tlbx = get_absolute_file_path("DDE_read_excel.dem.sce");
    demo_xls_file = demo_tlbx + 'DDE_DEMO.xls';
    disp('Opens Excel with ' + demo_xls_file);
    winopen(demo_xls_file);
  catch
    error(999,'Excel not found');
  end

  // Wait Excel
  sleep(2000);

  disp('Initialize Connection with Excel');
  res = ddeopen('Excel','');

  if res then
    // minimize Excel
    disp('Minimize Excel');
    res = ddeexec('[App.Minimize]');

    disp('read data L1C2 from Excel');
    Result = ddereq('L1C2');
    disp('L1C2: ""' + Result + '"" from ' + demo_xls_file);

    disp('read data L2C2 from Excel');
    Result = ddereq('L2C2');
    disp('L2C2: ""' + Result + '"" from ' + demo_xls_file);

    disp('read data L4C1 from Excel');
    Result = ddereq('L4C1');
    disp('L4C1: ""' + Result + '"" from ' + demo_xls_file);

    disp('read data L5C1 from Excel');
    Result = ddereq('L5C1');
    disp('L5C1: ""' + Result + '"" from ' + demo_xls_file);

    // maximize Excel
    disp('Maximize Excel');
    ddeexec('[App.Maximize]');

    disp('Quit Excel');
    ddeexec('[Quit()]');

    disp('Close Connection with Excel');
    res = ddeclose();
  end
endfunction
// ====================================================================
demo_read_excel();
clear demo_read_excel;
// ====================================================================


