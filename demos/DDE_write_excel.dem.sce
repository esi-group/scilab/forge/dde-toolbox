// ====================================================================
// Copyright DIGITEO 2008 - 2011
// Allan CORNET
// ====================================================================

function demo_write_excel()
  clc();
  try
    winopen('Excel');
  catch
    error(999,'Excel not found');
  end

  // Wait Excel
  sleep(2000);

  disp('Initialize Connection with Excel');
  res = ddeopen('Excel','[Classeur1]Feuil1');

  if res then
    // minimize Excel
    disp('Minimize Excel');
    ddeexec('[App.Minimize]')

    STR1 = '[1 2 3]';
    disp('Put ""' + STR1 + '"" in ' + 'L1C2');
    ddepoke('L1C2', STR1);

    STR2 = 'Scilab test';
    disp('Put ""' + STR2 + '"" in ' + 'L2C2');
    ddepoke('L2C2', STR2);

    disp('Change Font propertie for L2C2');
    command_line = '[SELECT(""L2C2"")][FONT.PROPERTIES(,""Bold"")]';
    ddeexec(command_line);

    EXCEL_FILE = TMPDIR + '\DDE_DEMO.xls';

    if fileinfo(EXCEL_FILE) <> [] then
      mdelete(EXCEL_FILE);
    end

    command_line = '[SAVE.AS(""' + EXCEL_FILE + '"",1,"""",FALSE,"""",FALSE)]';
    disp('Save excel file: ' + EXCEL_FILE);
    ddeexec(command_line);

    // maximize Excel
    disp('Maximize Excel');
    ddeexec('[App.Maximize]')

    disp('Quit Excel');
    ddeexec('[Quit()]');

    disp('Close Connection with Excel');
    ddeclose();

    if isfile(EXCEL_FILE) then
      winopen(EXCEL_FILE);
    end
    
  end
endfunction
// ====================================================================
demo_write_excel();
clear demo_write_excel;
// ====================================================================
