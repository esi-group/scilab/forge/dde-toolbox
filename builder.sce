// ====================================================================
// Copyright DIGITEO 2008 - 2011
// Allan CORNET
// ====================================================================
mode(-1);
lines(0);

function main_builder()
  // ====================================================================
  if getos() <> "Windows" then
   error(999,_("Only for Windows"));
   return
  end
  // ====================================================================
  // Check Scilab"s version
  // =============================================================================

  try
    v = getversion("scilab");
  catch
    error(gettext("Scilab 5.4 or more is required."));
  end

  if v(1) < 6 & v(2) < 4 then
    // new API in scilab 5.4
    error(gettext("Scilab 5.4 or more is required."));
  end

  // Check modules_manager module availability
  // =============================================================================

  if ~isdef("tbx_build_loader") then
    error(msprintf(gettext("%s module not installed."), "modules_manager"));
  end

  // ====================================================================
  TOOLBOX_NAME = "DDE_toolbox";
  TOOLBOX_TITLE = "DDE Toolbox";
  // ====================================================================
  toolbox_dir = get_absolute_file_path("builder.sce");

  tbx_builder_src(toolbox_dir);
  tbx_builder_gateway(toolbox_dir);
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
  tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);  

endfunction

main_builder();
clear main_builder;
// ====================================================================
