// ====================================================================
// Copyright DIGITEO 2008
// Allan CORNET
// ====================================================================
current_path = pwd();
root_path = get_absolute_file_path('run_tests_dde_toolbox.tst');
exec (root_path + 'loader.sce');
setenv('DDE_TOOLBOX_PATH',root_path);
test_run(root_path);
cd(current_path);
// ====================================================================
