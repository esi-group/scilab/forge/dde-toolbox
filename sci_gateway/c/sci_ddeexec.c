/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/* ==================================================================== */
#include "../../src/c/dde.h"
#include "sci_malloc.h"
#include "Scierror.h"
#include "localization.h"
#include "api_scilab.h"
/* ==================================================================== */
int sci_ddeexec(char *fname, void* pvApiCtx)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;
    int iParam2Value = -1; /* default value */

    Rhs = Max(0, Rhs);
    CheckRhs(1, 2);
    CheckLhs(0, 1);

    if (Rhs == 2)
    {
        int *piAddressVarTwo = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (isDoubleType(pvApiCtx, piAddressVarTwo))
        {
            double dVarTwo = NULL;

            if (isScalar(pvApiCtx, piAddressVarTwo))
            {
                if (getScalarDouble(pvApiCtx, piAddressVarTwo, &dVarTwo) == 0)
                {
                    iParam2Value = (int)(dVarTwo);
                }
            }
            else
            {
                Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"),fname,2);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A scalar expected.\n"),fname,2);
            return 0;
        }
    }

    if (Rhs == 1)
    {
        iParam2Value = -1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isStringType(pvApiCtx, piAddressVarOne))
    {
        BOOL paramoutBool = FALSE;
        wchar_t *pStVarOne = NULL;

        if ( !isScalar(pvApiCtx, piAddressVarOne) )
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A string expected.\n"),fname,1);
            return 0;
        }

        getAllocatedSingleWideString(pvApiCtx, piAddressVarOne, &pStVarOne);
        if (pStVarOne == NULL)
        {
            Scierror(999,_("%s: No more memory.\n"),fname);
            return 0;
        }


        paramoutBool = DDEExecute(pStVarOne, iParam2Value);

        freeAllocatedSingleWideString(pStVarOne); pStVarOne = NULL;

        createScalarBoolean(pvApiCtx, Rhs + 1, paramoutBool);

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument: String expected.\n"),fname);
    }

    return 0;
}
/* ==================================================================== */

