// ====================================================================
// Allan CORNET
// Copyright DIGITEO - 2008 - 2011
// ====================================================================
function build_gw()

  // PutLhsVar managed by user in sci_sum and in sci_sub
  // if you do not this variable, PutLhsVar is added
  // in gateway generated (default mode in scilab 4.x and 5.x)
  WITHOUT_AUTO_PUTLHSVAR = %t;

  funcList = ['ddeclose' , 'sci_ddeclose' ;
              'ddeexec' , 'sci_ddeexec' ;
              'ddeisopen' , 'sci_ddeisopen' ;
              'ddepoke' , 'sci_ddepoke' ;
              'ddereq' , 'sci_ddereq' ;
              'ddeopen' , 'sci_ddeopen'];

funcFiles = [ 'sci_ddeclose.c' ,
              'sci_ddeexec.c' ,
              'sci_ddeisopen.c' ,
              'sci_ddepoke.c' ,
              'sci_ddereq.c' ,
              'sci_ddeopen.c'];


tbx_build_gateway('gw_dde', funcList, funcFiles, ..
                  get_absolute_file_path('builder_gateway_c.sce'), ..
                  ['../../src/c/libdde_c']);

endfunction
// ====================================================================
build_gw();
clear build_gw;
// ====================================================================
