/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/* ==================================================================== */
#include "../../src/c/dde.h"
#include "BOOL.h"
#include "api_scilab.h"
/* ==================================================================== */
int sci_ddeisopen(char *fname, void* pvApiCtx)
{
    BOOL paramoutBool = FALSE;

    Rhs = Max(0,Rhs);
    CheckRhs(0, 0);
    CheckLhs(0, 1);

    paramoutBool = DDEIsOpen();
    createScalarBoolean(pvApiCtx, Rhs + 1, paramoutBool);

    LhsVar(1) = Rhs + 1;
    PutLhsVar();

    return 0;
}
/* ==================================================================== */

