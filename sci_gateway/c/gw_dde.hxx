#ifndef __GW_DDE_GW_HXX__
#define __GW_DDE_GW_HXX__

#ifdef _MSC_VER
#ifdef GW_DDE_GW_EXPORTS
#define GW_DDE_GW_IMPEXP __declspec(dllexport)
#else
#define GW_DDE_GW_IMPEXP __declspec(dllimport)
#endif
#else
#define GW_DDE_GW_IMPEXP
#endif

extern "C" GW_DDE_GW_IMPEXP int gw_dde(wchar_t* _pwstFuncName);



#endif /* __GW_DDE_GW_HXX__ */
