/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/* ==================================================================== */
#include <stdio.h>
#include "../../src/c/dde.h"
#include "sci_malloc.h"
#include "Scierror.h"
#include "localization.h"
#include "api_scilab.h"
/* ==================================================================== */
int sci_ddereq(char *fname, void* pvApiCtx)
{
    SciErr sciErr;

    int iParam2Value = -1;

    int *piAddressVarOne = NULL;

    Rhs = Max(0, Rhs);
    CheckRhs(1, 2);
    CheckLhs(0, 1);

    if (Rhs == 2)
    {
        int *piAddressVarTwo = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (isDoubleType(pvApiCtx, piAddressVarTwo))
        {
            double dVarTwo = NULL;

            if (isScalar(pvApiCtx, piAddressVarTwo))
            {
                if (getScalarDouble(pvApiCtx, piAddressVarTwo, &dVarTwo) == 0)
                {
                    iParam2Value = (int)(dVarTwo);
                }
            }
            else
            {
                Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"),fname,2);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A scalar expected.\n"),fname,2);
            return 0;
        }
    }

    if (Rhs == 1)
    {
        iParam2Value = -1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isStringType(pvApiCtx, piAddressVarOne))
    {
        BOOL bOK = FALSE;
        wchar_t *pStVarOne = NULL;
        wchar_t *Buffer = NULL;
        int lenBuffer = 0;

        if (!isScalar(pvApiCtx, piAddressVarOne))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A string expected.\n"), fname, 1);
            return 0;
        }

        getAllocatedSingleWideString(pvApiCtx, piAddressVarOne, &pStVarOne);
        if (pStVarOne == NULL)
        {
            Scierror(999,_("%s: No more memory.\n"),fname);
            return 0;
        }

        Buffer = DDERequest(pStVarOne, iParam2Value, TRUE, &lenBuffer, &bOK);
        freeAllocatedSingleWideString(pStVarOne);
        pStVarOne = NULL;

        if (bOK)
        {
            if (Buffer)
            {
                createSingleWideString(pvApiCtx, Rhs + 1, Buffer);
                FREE(Buffer);
                Buffer = NULL;
            }
            else
            {
                createSingleWideString(pvApiCtx, Rhs + 1, L"");
            }
        }
        else
        {
            createEmptyMatrix(pvApiCtx, Rhs + 1);
        }

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: A string expected.\n"),fname,1);
    }

    return 0;
}
/* ==================================================================== */
