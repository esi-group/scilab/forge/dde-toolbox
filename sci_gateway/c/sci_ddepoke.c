/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2008 - 2011 */
/* ==================================================================== */
#include "../../src/c/dde.h"
#include "sci_malloc.h"
#include "Scierror.h"
#include "localization.h"
#include "api_scilab.h"
/* ==================================================================== */
int sci_ddepoke(char *fname, void* pvApiCtx)
{
    SciErr sciErr;

    int iParam3Value = -1;

    int *piAddressVarOne = NULL;
    int *piAddressVarTwo = NULL;

    Rhs = Max(0, Rhs);
    CheckRhs(2, 3);
    CheckLhs(0, 1);

    if (Rhs == 3)
    {
        int *piAddressVarThree = NULL;

        sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
        if(sciErr.iErr)
        {
            printError(&sciErr, 0);
            return 0;
        }

        if (isDoubleType(pvApiCtx, piAddressVarThree))
        {
            double dVarThree = 0;

            if (isScalar(pvApiCtx, piAddressVarThree))
            {
                if (getScalarDouble(pvApiCtx, piAddressVarThree, &dVarThree) == 0)
                {
                    iParam3Value = (int)dVarThree;
                }
            }
            else
            {
                Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"),fname,3);
                return 0;
            }
        }
        else
        {
            Scierror(999,_("%s: Wrong type for input argument #%d: A scalar expected.\n"),fname,3);
            return 0;
        }
    }

    if (Rhs == 2)
    {
        iParam3Value = -1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if ( isStringType(pvApiCtx, piAddressVarOne) && isStringType(pvApiCtx, piAddressVarTwo) )
    {
        wchar_t *pStVarOne = NULL;
        wchar_t *pStVarTwo = NULL;

        BOOL paramoutBool = FALSE;

        if (!isScalar(pvApiCtx, piAddressVarOne))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A string expected.\n"),fname,1);
            return 0;
        }

        getAllocatedSingleWideString(pvApiCtx, piAddressVarOne, &pStVarOne);
        if (pStVarOne == NULL)
        {
            Scierror(999,_("%s: No more memory.\n"),fname);
            return 0;
        }

        if (!isScalar(pvApiCtx, piAddressVarTwo))
        {
            freeAllocatedSingleWideString(pStVarOne); pStVarOne = NULL;
            Scierror(999,_("%s: Wrong size for input argument #%d: A string expected.\n"),fname,2);
            return 0;
        }

        getAllocatedSingleWideString(pvApiCtx, piAddressVarTwo, &pStVarTwo);

        if (pStVarTwo == NULL)
        {
            freeAllocatedSingleWideString(pStVarOne); pStVarOne = NULL;
            Scierror(999,_("%s: No more memory.\n"),fname);
            return 0;
        }

        paramoutBool = DDEPoke(pStVarOne, pStVarTwo, iParam3Value);

        freeAllocatedSingleWideString(pStVarOne); pStVarOne = NULL;
        freeAllocatedSingleWideString(pStVarTwo); pStVarTwo = NULL;

        createScalarBoolean(pvApiCtx, Rhs + 1, paramoutBool);

        LhsVar(1) = Rhs + 1;
        PutLhsVar();
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument: String expected.\n"),fname);
    }

    return 0;
}
/* ==================================================================== */

