#ifndef __GW_DDE_GW_H__
#define __GW_DDE_GW_H__

#include "c_gateway_prototype.h"

C_GATEWAY_PROTOTYPE(sci_ddeclose);
C_GATEWAY_PROTOTYPE(sci_ddeexec);
C_GATEWAY_PROTOTYPE(sci_ddeisopen);
C_GATEWAY_PROTOTYPE(sci_ddeopen);
C_GATEWAY_PROTOTYPE(sci_ddepoke);
C_GATEWAY_PROTOTYPE(sci_ddereq);

#endif /* __GW_DDE_GW_H__ */
