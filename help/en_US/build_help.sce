// ====================================================================
// Copyright DIGITEO 2008 - 2011
// Allan CORNET
// ====================================================================
function build_help_en_US()
  help_lang_dir = get_absolute_file_path('build_help.sce');
  tbx_build_help(TOOLBOX_TITLE, help_lang_dir);
endfunction
// ====================================================================
build_help_en_US()
clear build_help_en_US;
// ====================================================================

