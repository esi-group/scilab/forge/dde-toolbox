// ====================================================================
// Allan CORNET
// Copyright DIGITEO 2008 - 2011
// ====================================================================
root_path = getenv("DDE_TOOLBOX_PATH","");
if  root_path <> "" then
  exec(root_path+"loader.sce");
end
// ====================================================================
test_path = root_path + "tests" + filesep() + "unit_tests" + filesep();
// ====================================================================

excel_file = test_path + "DDE_test1.xls";
ierr = execstr("winopen(""" + excel_file +""");","errcatch");
assert_checkequal(ierr, 0);

// Wait Excel
sleep(2000);

res = ddeopen("Excel","");
assert_checkequal(res, %T);

res = ddeexec("[App.Minimize]");
assert_checkequal(res, %T);

Result = ddereq("L1C2");
assert_checkequal(Result, "2");

Result = ddereq("L2C2");
assert_checkequal(Result, "5");

Result = ddereq("L4C1");
assert_checkequal(Result, "Scilab string");

res = ddeexec("[App.Maximize]");
assert_checkequal(res, %T);

res = ddeexec("[Quit()]");
assert_checkequal(res, %T);

res = ddeclose();
assert_checkequal(res, %T);

// ====================================================================
